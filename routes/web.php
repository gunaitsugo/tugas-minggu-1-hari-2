<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', 'UserController@test')->middleware('admin');
Route::middleware(['auth', 'admin'])->group(function(){
    Route::get('/route-1', 'UserController@user')->name('route-1');
    Route::get('/route-2', 'UserController@user')->name('route-2');
    Route::get('/route-3', 'UserController@user')->name('route-3');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
