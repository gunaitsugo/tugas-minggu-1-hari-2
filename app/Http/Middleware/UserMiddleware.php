<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Auth;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->route()->uri()=='route-1'){
            if(Auth::user()->cekUser() == 'superadmin'){
                return  $next($request);
            }
        } else if($request->route()->uri()=='route-2'){
            if(Auth::user()->cekUser() == 'admin' || Auth::user()->cekUser() == 'superadmin'){
                return  $next($request);
            }
        }  else if($request->route()->uri()=='route-3'){
            if(Auth::user()->cekUser() == 'guest' || Auth::user()->cekUser() == 'admin' || Auth::user()->cekUser() == 'superadmin'){
                return  $next($request);
            }
        }
        abort(403);
        
        
    }
}
