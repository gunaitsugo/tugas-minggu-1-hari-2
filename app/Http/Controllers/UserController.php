<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Route;

class UserController extends Controller
{
    public function test(){
        return 'Berhasil masuk';
    }

    public function user(){
        return 'Selamat Datang di Halaman '.Route::currentRouteName().', '.Auth::user()->name;
    }
}
